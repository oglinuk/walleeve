console.log(`"Who controls the past controls the future. Who controls the present controls the past." ~ George Orwell`);
let count = 1;

function append_row() {
	$(".items").append(
	`<br>
		<div id=${count}>
			<input class="delete" id="${count}" type="button" onclick="remove_row(this.id)" value="X" />&nbsp;&nbsp;
			<label style="margin-top: 1.5%;">Item #: </label>
			<input type="number" step="1" min="0" max="999999" style="width: 5%;" name="item_number" required />
			<label style="margin-top: 1.5%;">Description: </label>
			<input type="text" style="width: 30%;" name="description" required />
			<label style="margin-top: 1.5%;">Bid $: </label>
			<input type="number" step="0.01" min="1" max="999999" style="width: 5%;" name="bid" required />
		</div>`
	);
	count = count + 1;
};

function remove_row(eleID) {
	count = count - 1;
	document.getElementById(eleID).remove();
};

function call_alert() {
	alert('Your bid has been accepted. Thank you and have a nice day!');
};


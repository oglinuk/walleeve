from app import app, db
from flask import render_template, request
import time

@app.route('/', methods=['GET', 'POST'])
def serve_index():
    if request.method == 'POST':
        # https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data

        name = request.values.get('name')
        phone = request.values.get('phone')
        item_number = request.values.getlist('item_number')
        description = request.values.getlist('description')
        bid = request.values.getlist('bid')

        for i in range(len(item_number)):
            db.insert('INSERT INTO tblAuction(item_number, description, bid, name, phone, datetimestamp) VALUES(?, ?, ?, ?, ?, ?);',
                [item_number[i], description[i], bid[i], name, phone, time.time()])
    
    return render_template('/index.html')

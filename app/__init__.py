import csv
import os
from flask import Flask
from flask_cors import CORS
import random
import sqlite3
import time

##### For testing only #####
names = ['alice', 'bob', 'candice', 'dante', 'eliot', 'farquad', 'gijs',
'helios', 'igor', 'juan', 'kody', 'leonard', 'magnus', 'ned', 'oscar',
'phillip']

def insert_dummy_data():
	for name in names:
		db.insert('INSERT INTO tblAuction(item_number, description, bid, name, phone, datetimestamp) VALUES(?, ?, ?, ?, ?, ?);', [random.randint(1, 5), 'something', random.randint(1, 100), random.choice(names), random.randint(1000000, 9999999), time.time()])
##### For testing only #####

# SQLite3 Database Class
class SQLite3DB():
	def __init__(self, name, schema):
		self.count = 0
		self.db_name = name
		self.db_schema = schema
		self.db_conn = sqlite3.connect(self.db_name, check_same_thread=False)
		# https://docs.python.org/3/library/sqlite3.html#sqlite3.Cursor
		self.db_cursor = self.db_conn.cursor()
		self.create_db()

	def create_db(self):
		self.db_cursor.execute(self.db_schema)
		self.db_conn.commit()

	# This is dangerous, but because we are hardcoding the SQL statements
	# its ok. You should never accept an abitrary query.
	def insert(self, query, entry):
		# Example query -> "INSERT INTO tbl(some, thing) VALUES(?, ?)"
		self.db_cursor.execute(query, ([v for v in entry]))

		# https://docs.python.org/3/library/sqlite3.html#sqlite3.Connection.commit
		self.db_conn.commit()
   
	def get_all(self, query):
		self.db_cursor.execute(query)
		return self.db_cursor.fetchall()

# Database Object. In most cases this would be abstracted behind a data
# access layer (a REST API wrapping the database), but in this use case
# the data is not wanting to be persisted. Sqlite is the best option.
db = SQLite3DB('nec-auction.db', '''
    CREATE TABLE IF NOT EXISTS tblAuction(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        phone TEXT,
        item_number TEXT,
        description TEXT,
        bid INT,
        datetimestamp TEXT
    );
''')

##### For testing purposes ONLY #####
#insert_dummy_data()

##### Flask Application #####
app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config')
CORS(app) # Cause CORS ...

from app.controllers import *

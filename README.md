# Walleeve

## Getting Started

### via Python 3.9

* `pip3 install -r requirements.txt`
* `python3 run.py`

### via Docker

* `docker build -t walleeve .`
* `docker run -p 7777:7777 walleeve`

## Routes

* `/csv` - exports all database entries as a csv
* `/csv/groupby/name` - exports all database entries grouped by name - TODO
* `/csv/groupby/item` - exports all database entries grouped by item #
* `/top/bids/<n>` - exports all top bids for `<n>` items

# TODO

* [X] Create `/top/bids/<n>` route
* [X] Create `/csv/groupby/item` route
* [ ] Create `/csv/groupby/name` route
* [X] Document
* [ ] Create CI/CD pipeline

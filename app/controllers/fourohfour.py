from app import app
from flask import render_template, jsonify

# Error Handler
@app.errorhandler(404)
def serve_error(err):
    return render_template('/404.html', error=err)

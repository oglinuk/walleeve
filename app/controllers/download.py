from app import app, db
import csv
from flask import make_response
from io import StringIO
import time

# new_csv returns a new in-memory csv
def new_csv(query, headers, name):
  # https://docs.python.org/3/library/io.html#io.StringIO
	buf = StringIO()
  # https://docs.python.org/3/library/csv.html#csv.writer
	csv_w = csv.writer(buf)
  # Write column names of csv file
	csv_w.writerow(headers)

	for row in db.get_all(query):
		#       id  name   phone item # desc     bid      time
    # row = '1, gijs, 3923054, 3, something, 20, 1604483066.91224'
    #        v   v       v     v      v       v         v
		e = [v for v in row]
		del e[0] # Dont need id

		# TODO: Replace below with a better way
		# Re-arranging order of list according to nec preference
		# item_number, description, bid, name, phone, time
		e[3], e[4], e[0], e[1], e[2], e[5] = e[0], e[1], e[2], e[3], e[4], e[5]

		csv_w.writerow(e)

  # https://flask.palletsprojects.com/en/1.1.x/api/#flask.make_response
	out = make_response(buf.getvalue())
	out.headers['Content-Disposition'] = f'attachment; filename={{name}}.csv'
	out.headers['Content-Type'] = 'text/csv'
	return out

# /csv creates and returns a csv of all data
@app.route('/csv')
def serve_csv_download():
	return new_csv('SELECT * FROM tblAuction;', [
		'item_number',
		'description',
		'bid',
		'name',
		'phone',
		'time'
	], 'all')

# TODO: nec would like a csv that is grouped by name and includes an
# additional 'total' column with the total amount of all bids made by one
# individual. This will need some thinking around 'new_csv'.
#@app.route('/csv/groupby/name')
#def serve_csv_download_groupbyname():
#	return new_csv(,'SELECT * FROM tblAuction ORDER BY(name) ASC;', [
#		'item_number',
#		'description',
#		'bid',
#		'total',
#		'name',
#		'phone',
#		'time',
#	], 'groupby-name')

@app.route('/csv/groupby/item')
def serve_csv_download_groupbyitem():
	return new_csv('SELECT * FROM tblAuction ORDER BY(item_number) ASC;', [
		'item_number',
		'description',
		'bid',
		'name',
		'phone',
		'time',
	], 'groupby-item')

from app import app, db
import csv
from flask import make_response, request
from io import StringIO

@app.route('/top/bids/<n>')
def serve_top_bids(n):
	topbids = []

	for i in range(int(n)):
		topbids.append(db.get_all(f'''
			SELECT name, phone, item_number, description, MAX(bid), datetimestamp
			FROM tblAuction
			WHERE item_number = {i}
		''')[0])

	buffer = StringIO()
	csv_w = csv.writer(buffer)

	csv_w.writerow([
		'name',
		'phone',
		'item_number',
		'description',
		'bid',
		'time'
	])

	for row in topbids:
		csv_w.writerow([v for v in row])

	out = make_response(buffer.getvalue())
	out.headers['Content-Disposition'] = 'attachment; filename=topbids.csv'
	out.headers['Content-Type'] = 'text/csv'
	return out
